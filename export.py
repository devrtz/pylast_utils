#
# pylast_utils -
#     Python utilities for pylast:
#     A Python interface to Last.fm and Libre.fm
#
# Copyright 2020 devrtz
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# https://gitlab.com/devrtz/pylast_utils

from optparse import OptionParser
import datetime
import calendar
import os
import csv
import time
import pylast

try:
    API_KEY = os.environ['LASTFM_API_KEY']
    API_SECRET = os.environ['LASTFM_API_SECRET']
    USERNAME = os.environ['LASTFM_USERNAME']
    PASSWORD_HASH = pylast.md5(os.environ['LASTFM_PASSWORD'])

except KeyError:
    print ("""Error! You need to set the following environment variables
LASTFM_API_KEY, LASTFM_API_SECRET, LASTFM_USERNAME and LASTFM_PASSWORD
Exiting""")
    exit (1)


def get_tracks(user, year=None, limit_rate=False, rate_limit_time=300, rate_limit_batch=500):
    if year:
        date_from = datetime.date(year, 1, 1)
        date_from_secs_epoch = calendar.timegm(date_from.timetuple())
        date_to = datetime.date(year, 12, 31)
        date_to_secs_epoch = calendar.timegm(date_to.timetuple())
    else:
        date_to_secs_epoch = int(time.time())
        date_from_secs_epoch = 0


    if not limit_rate:
        if not type(rate_limit_time) is int or not type(rate_limit_batch) is int:
            raise TypeError ('rate_limit_time and rate_limit_batch must be integers!')
        if not rate_limit_time > 0 or not rate_limit_batch > 0:
            raise ValueError ('rate_limit_time and rate_limit_time must be greather than zero!')
        return user.get_recent_tracks(limit=None,
                                      time_from=date_from_secs_epoch,
                                      time_to=date_to_secs_epoch)
    done = False
    results = []
    while not done:
        res = user.get_recent_tracks(limit=rate_limit_batch,
                                     time_from=date_from_secs_epoch,
                                     time_to=date_to_secs_epoch)
        results.extend(res)
        if len(res) == 0:
            done = True
        else:
            time.sleep(rate_limit_time)
            date_to_secs_epoch = res[-1].timestamp

    return results

def scrobble_args_from_played_track(track, use_mbid=False, use_duration=True):
    duration = None
    mbid = None
    if use_duration:
        try:
            duration = track.track.get_duration()
        # during execution I got pylast.WSError, xml.parsers.expat.ExpatError, pylast.MalformedResponseError
        # since I don't know what other exceptions could be raised I gotta catch 'em all
        except Exception:
            print ("Could not get duration for track {} from {}".format(
                track.track.title,
                track.track.artist.name))


    if use_mbid:
        try:
            mbid = track.track.get_mbid()
        except pylast.WSError:
            print ("Could not get MbID for track {} from {}".format(
                track.track.title,
                track.track.artist.name))


    result = {
        "artist" : track.track.artist.name,
        "title" : track.track.title,
        "timestamp" : track.timestamp,
        "album" : track.album,
        "album_artist" : track.track.artist.name,
        "track_number" : None,
        "duration" : duration,
        "stream_id" : None,
        "context" : None,
        "mbid" : mbid
    }
    return result

def main():
    parser = OptionParser()
    parser.add_option("-f", "--file",
                      action="store", type="string", dest="filename",
                      help="Filename to write scrapped data into")
    parser.add_option("-y", "--year",
                      action="store", type="int", dest="year",
                      help="Get records for this year")
    parser.add_option("-r", "--rate-limit",
                      action="store_true", dest="use_rate_limit",
                      help="Whether to use rate limits")
    parser.add_option("-n", "--rate-number",
                      action="store", type="int", dest="rate_limit_batch",
                      help="How many records should be fetched at once")
    parser.add_option("-t", "--rate-time",
                      action="store", type="int", dest="rate_limit_time",
                      help="How long to sleep after fetching a batch of records")
    parser.add_option("-m", "--mbid",
                      action="store_true", dest="use_mbid",
                      help="Whether to query MusicBrainz ID for albums/tracks (this is an addtional query to LastFM)")
    parser.add_option("-d", "--duration",
                      action="store_true", dest="use_duration",
                      help="Whether to query track duration (this is an additional query to LastFM)")

    parser.set_defaults(
        filename=None,
        year=None,
        use_rate_limit=False,
        rate_limit_batch=999,
        rate_limit_time=60,
        use_mbid=False,
        use_duration=False)

    (options, args) = parser.parse_args()

    if options.filename is None:
        print('Error! Filename must be provided! Exiting')
        exit(1)

    network = pylast.LastFMNetwork(api_key=API_KEY, api_secret=API_SECRET, username=USERNAME, password_hash=PASSWORD_HASH)
    user = network.get_user(USERNAME)

    if options.rate_limit_batch > 999:
        print('pylast limits number of fetched tracks to 999')
        options.rate_limit_batch = 999

    time_start = time.time()
    print("Fetching started at {}".format(datetime.datetime.now()))

    recent_tracks = get_tracks(user,
                               year=options.year,
                               limit_rate=options.use_rate_limit,
                               rate_limit_time=options.rate_limit_time,
                               rate_limit_batch=options.rate_limit_batch)

    time_end = time.time()
    time_duration = time_end - time_start
    n_tracks = len(recent_tracks)
    print("Fetched {} tracks in {} seconds".format(n_tracks,
                                                   time_duration))

    if recent_tracks and len(recent_tracks) > 0:
        d = scrobble_args_from_played_track (recent_tracks[0])
        if options.use_rate_limit:
            time.sleep(options.rate_limit_time)

        with open(options.filename, "w") as f:
            csvwriter = csv.DictWriter(f, delimiter=';', fieldnames=d.keys())
            csvwriter.writeheader()
            query_count = 0

            for track in recent_tracks:
                d = scrobble_args_from_played_track(track,
                                                    use_mbid=options.use_mbid,
                                                    use_duration=options.use_duration)
                csvwriter.writerow(d)
                if options.use_mbid:
                    query_count += 1
                if options.use_duration:
                    query_count += 1

                if options.use_rate_limit and query_count >= options.rate_limit_batch:
                    query_count = 0
                    time.sleep(options.rate_limit_time)


if __name__ == "__main__":
    main()
