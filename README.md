# Introduction

I have a large library (>150k scrobbles) on last.fm which I wanted to export to libre.fm.
I have come accross the very nice (pylast)[https://github.com/pylast/pylast] utility 
and it seemed suitable for both exporting the data from last.fm and importing it into libre.fm

# Export from last.fm

You can use the export script as follows:
```
LASTFM_API_KEY="<YOUR_API_KEY>" LASTFM_API_SECRET="<YOUR_API_SECRET>" LASTFM_USERNAME="<YOUR_USER>" LASTFM_PASSWORD="<YOUR_PASSWORD>" python export.py -f export.csv -r
```
I do not know the rate limits for using the last.fm API. But with the defaults in this script (1000 requests then 5 minutes pause).
It took me nearly two days to export ~160k scrobbles from my library.
Most of this time was spent getting the track durations.
This time can be cutdown if you don't want to get the track duration to a few hours (do not specify `-d` flag).

# Import

TODO: Still need to code this one up. It will mass scrobble the previously exported data

# Dependencies

The scripts only depend on `pylast`. You should be able to grab it from your distribution or with `pip install pylast`.
